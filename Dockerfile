FROM node:10.12-alpine

# Create app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install
# If you are building your code for production
# RUN npm install --only=production
RUN apk add --update openssl

# Bundle app source
COPY . .

# Ask for SECRET_KEY
ENV SECRET_KEY="pocketloan_secret_key_123"

RUN openssl enc -in .env.enc -out .env -d -aes256 -k "$SECRET_KEY"

CMD [ "npm", "start"]