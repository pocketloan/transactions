require('dotenv').config()
const assert = require('assert')
const Mongo = require.main.require('server/dao/mongo')

describe('MongoDao', () => {
    const mongo = new Mongo()
    describe('databaseOptions()', () => {
        it('should return an object of the mongo configurations', async () => {
            let options = await mongo.databaseOptions()
            assert.deepEqual(options, {})
        })
    })
})
