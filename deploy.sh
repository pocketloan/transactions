#!/bin/bash -ex

# Set environment variables
LATEST="latest"
SHA=$(git rev-parse HEAD)

# Pushes all specified image tags to dockerhub
dockerhub () {
    docker build -t pocketloan/transactions:$1 -t pocketloan/transactions:$2 .
    echo "--------------------------------"
    echo " Pushing to dockerhub at $(date +"%T")"
    echo "--------------------------------"
    docker push pocketloan/transactions:$1 && docker push pocketloan/transactions:$2
    echo "--------------------------------"
    echo " Pushed to dockerhub at $(date +"%T")"
    echo "-------------------------------- \n"
}

# Deploys application to kubernetes cluster on google cloud platform
deploy () {
    echo "--------------------------------"
    echo " Deploying to gcloud at $(date +"%T")"
    echo "--------------------------------"
}

dockerhub $SHA $LATEST || echo "Something went wrong pushing to dockerhub"
echo ""
deploy || echo "Something went wrong while deploying to gcloud"