const errorMessages = {
    UNHEALTHY_APP: {
        message: 'app is unhealthy. it cannot connect to th database',
        status: 500,
        title: 'Unhealthy Application'
    },
    UNAUTHORIZED_REQUEST: {
        status: 404,
        message: 'Unauthorized Request',
        title: 'Unauthorized'
    },
    NOTFOUND: {
        status: 404,
        message: 'Resource does not exists or has been deleted',
        title: 'Not Found'
    },
    CONFLICT: {
        status: 409,
        message: 'Resource already exists',
        title: 'Conflict'
    },
    PAYMENT_REQUIRED: {
        status: 402,
        message: 'Payment required to purchase service',
        title: 'Payment Required'
    },
    FORBIDDEN: {
        status: 403,
        message: 'Resource required is forbbiden',
        title: 'Forbidden Request'
    },
    SERVER_ERROR: {
        status: 500,
        message: 'Internal server error',
        title: 'Server Error'
    },
    UNKNOWN_ERROR: {
        status: 520,
        message: 'Unknown due to an invalid response',
        title: 'Unknown Error'
    },
    BAD_REQUEST: {
        status: 400,
        message: 'Bad request due to unknown or missing query field(s)',
        title: 'Bad Request'
    }
}

module.exports = errorMessages