const axios = require('../utils/axios')
const { pocketloanActivity } = require('../../config')

class Activity {
    constructor(options = {}) {
        this.options = options
    }
    /**
     * @param {Object[String]} userId
     * @param {Object[String]} transactionId
     * @returns {Object} activity - activity object
     * creates a new activity tracker for user
     */
    async createActivity({ userId, transactionId }) {
        return axios.post(`${pocketloanActivity}/v1/activities/`, { userId, transactionId })
            .then(response => response.data)
            .catch(err => { throw err })
    }
    /**
     * @param {Object[String]} transactionId
     * @param {Object} fields - fields to be updated
     * @returns {Object} activity - activity object
     * updates a transaction activity field by pushing int array
     */
    async updateTransactionActivity({ transactionId, fields }) {
        const { title, content } = fields
        return axios.patch(`${pocketloanActivity}/v1/activities/transactions/${transactionId}`, {
            transactionActivity: { title, content }
        }).then((response) => {
            return response.data.activity
        }).catch(err => { throw err })
    }
    /**
     * @param {Object[String]} userId
     * @param {Object} fields - fields to be updated
     * @returns {Object} activity - activity object
     * updates a user activity field by pushing int array
     */
    async updateUserActivity({ userId, fields }) {
        const { title, content } = fields
        return axios.patch(`${pocketloanActivity}/v1/activities/users/${userId}`, {
            userActivity: { title, content }
        }).then((response) => {
            return response.data.activity
        }).catch(err => { throw err })
    }
    /**
     * @returns {[Object]} activities - all previous activities 
     * returns all activities
     */
    async getActivities() {
        return axios.get(`${pocketloanActivity}/v1/activities`).then((response) => {
            return response.data.activities
        }).catch(err => { throw err })
    }
}

module.exports = Activity