const _ = require('lodash')
const jwtDecode = require('jwt-decode')
const { UNAUTHORIZED_REQUEST } = require('../errorMessages')
const { hasAccess, env } = require('../../config')

/**
 * @class {Object} Middleware
 * @constructor {Object} Options
 * Middleware to handle user authentication and logging
 */
class Middleware {
    constructor(options = {}) {
        this.options = options
    }

    /**
     * @param {Object} ctx koa context
     * @param {Function} next request.next
     * checks if user has access to application
     */
    async handleAuth(ctx, next) {
        const url = ctx.request.url
        const isValid = await this._verifyAuthToken(ctx.request) || await this._isAllowedPath(url) || env(ctx.request) === 'development'
        if (isValid) {
            await next()
        } else {
            ctx.status = 401
            ctx.body = UNAUTHORIZED_REQUEST
        }
    }

    /**
     * @param {Object} req koa context.request
     * @return {Boolean} verifies auth token
     * checks if authorization token is valid
     */
    async _verifyAuthToken(req) {
        const token = req.headers.authorization
        return (token) ? await this._validateToken(jwtDecode(token)) : false
    }

    /**
     * @param {Object} decoded jwt decoded object
     * @return {Boolean} validates auth token
     * checks if authorization token is generated from a valid pocketloan service
     */
    async _validateToken(decoded) {
        let isValid = false
        const obj = _.pick(decoded, ['sub', 'subType', 'aud', 'iss'])
        hasAccess.forEach(access => {
            if (_.isEqual(access, obj)) {
                isValid = true
            }
        })
        return isValid
    }

    async _isAllowedPath(url) {
        return url.includes('health') || url.includes('openapi') || url === '/'
    }
}

// exports the Middleware class
module.exports = Middleware