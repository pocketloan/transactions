// Require dependencies
const { isObject, isEmpty } = require('lodash')
const { stripeSecretKey } = require('../../config')
const stripe = require('stripe')(stripeSecretKey)

/**
 * Transaction Service Class
 * Handles all stripe transactions
 * Api's gateway to cimmunicate with stripe 
 * References -> https://stripe.com/docs/api#intro
 * @constructor {Object} options
 * @param {Object} constructor - [transactionDao]
 */
class TransactionService {
    constructor(options = {}) {
        this.options = options
        this.stripe = stripe
    }

    /**
     * Creates a new stripe customer for recurring charges
     * @param {String} cardToken - user card id token
     * @returns {Object} customer - a new customer object instance
     */
    async createCustomer(params) {
        try {
            return this.stripe.customers.create(params)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a stripe customer for recurring charges
     * @param {String} customerId - customers id
     * @param {Object} params - fields to be updated
     * @returns {Object} customer - a new customer object instance
     */
    async updateCustomer(customerId, params) {
        try {
            return this.stripe.customers.update(customerId, params)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves a particular customer by id
     * @param {String} customerId - customers unique id
     * @returns {Object} customer - customer object instance
     */
    async retrieveCustomer(customerId) {
        try {
            if (!isEmpty(customerId)) {
                return this.stripe.customers.retrieve(customerId)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a particular customer by id
     * @param {String} customerId - customers unique id
     * @param {Object} fields - customers fields to be updated
     * @returns {Object} customer - updated customer object instance
     */
    async updateCustomer(customerId, fields) {
        try {
            if (isObject(fields) && !isEmpty(customerId)) {
                return this.stripe.customers.update(customerId, fields)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Deletes a particular customer by id
     * @param {String} customerId - customers unique id
     * @returns {Object} confirmation
     */
    async deleteCustomer(customerId) {
        try {
            if (!isEmpty(customerId)) {
                return this.stripe.customers.del(customerId)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all customers from stripe
     * @param {Number} limit - size of the list of most recent customers
     * @returns {Object} list of limit customers (limit default is 10)
     */
    async retrieveCustomers(limit = 10) {
        try {
            return this.stripe.customers.list({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Creates a charge on the users bank account / debit card
     * @param {Object} transaction - recent transaction that took place
     * @returns {Object} charge object
     */
    async createCharge(transaction, { amount, currency }) {
        try {
            const { email, id } = transaction
            return this.stripe.charges.create({
                amount,
                currency,
                customer: id,
                description: `charge for ${email}`
            })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves a charge object by users charge id
     * @param {String} chargeId - users charge id
     * @returns {Object} charge object
     */
    async retrieveCharge(chargeId) {
        try {
            if (!isEmpty(chargeId)) {
                return this.stripe.charges.retrieve(chargeId)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all charges
     * @param {Number} limit - size of the list of most recent charges
     * @returns {[Object]} list of charge objects
     */
    async retrieveCharges(limit = 10) {
        try {
            return this.stripe.charges.list({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a particular charge by id
     * @param {String} chargeId - users unique charge id
     * @param {Object} fields - charge fields to be updated
     * @returns {Object} charge object
     */
    async updateCharge(chargeId, fields) {
        try {
            if (isObject(fields) && !isEmpty(chargeId)) {
                return this.stripe.charges.update(chargeId, fields)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all balances
     * @param {String} accountId - users accountId or null
     * @returns {Object} balance object
     */
    async retrieveBalance(stripe_account) {
        try {
            if (isEmpty(stripe_account)) {
                return this.stripe.balance.retrieve()
            }
            return this.stripe.balance.retrieve({ stripe_account })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a particular balance transaction by id
     * @param {String} balanceTransactionId - balance transaction Id
     * @returns {Object} balance transaction object
     */
    async retrieveBalanceTransaction(balanceTransactionId) {
        try {
            if (!isEmpty(balanceTransactionId)) {
                return this.stripe.balance.retrieveTransaction(balanceTransactionId)
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all balances
     * @param {Number} limit - size of the list of most recent balance transactons
     * @returns {[Object]} a list of balance transactions object
     */
    async retrieveBalanceTransactions(limit = 10) {
        try {
            return this.stripe.balance.listTransactions({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Creates a payout (sends money to a particular)
     * @param {payout[amount]} amount - amount of money to be paid
     * @param {payout[currency]} currency - currency to be paid to the user i.e cad, usd etc .....
     * @param {payout[destination]} destination - bankAccountId, cardId or account the money should be paid to
     * @returns {Object} a new payout object
     */
    async createPayout({ amount, currency, destination, source_type, stripeAccount }) {
        try {
            const method = (source_type === 'card') ? 'instant' : 'standard'
            return this.stripe.payouts.create({ amount, currency, destination, source_type, method }, stripeAccount)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves a payout object by id 
     * @param {String} payoutId - payout object id
     * @returns {Object} payout object
     */
    async retrievePayout(payoutId) {
        try {
            return this.stripe.payouts.retrieve(payoutId)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a payout object by id 
     * @param {String} payoutId - payout object unique identification
     * @param {Object} fields - payout fields to be updated
     * @returns {Object} payout object
     */
    async updatePayout(payoutId, fields) {
        try {
            return this.stripe.payouts.update(payoutId, fields)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Cancels a payout
     * @param {String} payoutId - payout object unique identification
     * @returns {Object} payout object cancellation confirmation
     */
    async cancelPayout(payoutId) {
        try {
            return this.stripe.payouts.cancel(payoutId)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all payouts restricted by limit
     * @param {Number} limit - size of the list of most recent payouts
     * @returns {[Object]} a list of payout objects
     */
    async retrievePayouts(limit = 10) {
        try {
            return this.stripe.payouts.list({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Creates a new account for easier transactions - https://stripe.com/docs/api#account
     * @param {account[type]} type - https://stripe.com/connect/account-types
     * @param {account[country]} country - users country
     * @param {account[email]} email - users email address
     * @returns {Object} 
     */
    async createAccount({ type = 'custom', country, email }) {
        try {
            return this.stripe.accounts.create({ type, country, email })
        } catch (err) {
            this._handleError(err)
        }
    }

    /** Creates a new bank or card token for secure transactions
     * @param {Object} params - https://stripe.com/docs/api#tokens
     * @returns {Object} a card or bank token object
     */
    async createToken(params) {
        try {
            const { tokenType } = params
            const validTokenTypes = ['bank', 'card']
            if (isEmpty(tokenType) && !validTokenTypes.includes(tokenType)) {
                throw new Error(`Expected a token type be a string of ${validTokenTypes} but got ${typeof tokenType}`)
            }
            if (tokenType === 'card') {
                const { number, exp_month, exp_year, cvc } = params
                return this.stripe.tokens.create({
                    card: { number, exp_month, exp_year, cvc }
                })
            } else {
                const { country, currency, account_holder_name, account_holder_type, routing_number, account_number } = params
                return this.stripe.tokens.create({
                    bank_account: { country, currency, account_holder_name, account_holder_type, routing_number, account_number }
                })
            }
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates an account object by id 
     * @param {String} accountId - account object id
     * @param {Object} fields - account fields to be updated
     * @returns {Object} account object
     */
    async updateAccount(accountId, fields) {
        try {
            return this.stripe.accounts.update(accountId, fields)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves an account object by id 
     * @param {String} accountId - account object id
     * @returns {Object} account object
     */
    async retrieveAccount(accountId) {
        try {
            return this.stripe.accounts.retrieve(accountId)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Deletes a particular account by id
     * @param {String} accountId - account object id
     * @returns {Object} confirmation
     */
    async deleteAccount(accountId) {
        try {
            return this.stripe.accounts.del(accountId)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all accounts restricted by limit
     * @param {Number} limit - size of the list of most recent accounts
     * @returns {[Object]} a list of account objects
     */
    async retrieveAccounts(limit = 10) {
        try {
            return this.stripe.accounts.list({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all accounts restricted by limit
     * @param {String} accountId - users stripe accountId
     * @param {String} bankAccountToken - bank account token of user i.e reference access to users bank account
     * @returns {Object} an external account Object
     */
    async createExternalAccount(accountId, bankAccountToken) {
        try {
            return this.stripe.accounts.createExternalAccount(accountId, { external_account: bankAccountToken })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves a card token object by id 
     * @param {String} tokenId - token object id
     * @returns {Object} token object
     */
    async retrieveToken(tokenId) {
        try {
            return this.stripe.tokens.retrieve(tokenId)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Creates a transfer to send funds to a connected stripe account 
     * @param {[Number]} amount- amount to be transfered
     * @param {[String]} currency - currency
     * @returns {Object} Transfer Object
     */
    async createTransfer({ amount, currency, destination }) {
        try {
            return this.stripe.transfers.create({
                amount,
                currency: currency || 'cad',
                destination
            })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves a transfer by transfer Id
     * @param {String} id - transfer id
     * @returns {Object} Transfer Object
     */
    async retrieveTransfer(id) {
        try {
            return this.stripe.transfers.retrieve(id)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Updates a transfer by id
     * @param {String} id - transfer id
     * @param {String} fieldsUpdated - transfer object fields to be updated
     * @returns {Object} Transfer Object
     */
    async updateTransfer(id, fieldsUpdated) {
        try {
            return this.stripe.transfers.update(id, fieldsUpdated)
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Retrieves all transfers restricted by limit
     * @param {Number} limit - size of the list of most recent transfers
     * @returns {[Object]} a list of transfer objects
     */
    async retrieveTransfers(limit = 10) {
        try {
            return this.stripe.transfers.list({ limit })
        } catch (err) {
            this._handleError(err)
        }
    }

    /**
     * Handles, Logs and Alert errors as notifications
     * References -> https://stripe.com/docs/error-codes
     * @param {Error} err - error object
     */
    _handleError(err) {
        if (err) {
            switch (err.type) {
                case 'StripeCardError':
                    // A declined card error
                    err.message; // => e.g. "Your card's expiration year is invalid."
                    break;
                case 'RateLimitError':
                    // Too many requests made to the API too quickly
                    break;
                case 'StripeInvalidRequestError':
                    console.log(err)
                    // Invalid parameters were supplied to Stripe's API
                    break;
                case 'StripeAPIError':
                    // An error occurred internally with Stripe's API
                    break;
                case 'StripeConnectionError':
                    // Some kind of error occurred during the HTTPS communication
                    break;
                case 'StripeAuthenticationError':
                    // You probably used an incorrect API key
                    break;
                default:
                    console.error(err)
                    break;
            }
        }
    }
}

module.exports = TransactionService