// Require dependencies
const Mongo = require('../dao/mongo')

/**
 * @class {Object} HealthService
 * @param {Object} options
 * checks the health of the pocketloan-api application 
 */
class HealthService {
    constructor(options = {}) {
        this.options = options
        this.mongo = new Mongo()
    }

    /**
     * @param {Null}
     * @returns {Boolean} boolean
     * returns true or false base on the health of the appliaction
     */
    async isHealthy() {
        return this.mongo.connect()
    }
    /**
     * @param {Null}
     * @returns {Object} health service options
     * return health service configurations
     */
    async getOptions() {
        return this.options
    }
}

// Exports the HealthService
module.exports = HealthService