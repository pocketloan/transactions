// Require dependencies
const mongoose = require('mongoose')

// creates and intitializes the payments schema
const TransactionSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true,
        index: {
            unique: true
        }
    },
    customerId: {
        type: String
    },
    accounts: [{
        accountId: {
            type: String,
            required: true
        },
    }],
    cardTokens: [{
        cardTokenId: {
            type: String,
            required: true
        }
    }],
    bankAccounts: [{
        bankAccountTokenId: {
            type: String,
            required: true
        }
    }],
    payouts: [{
        payoutId: {
            type: String,
            required: true
        },
    }],
    charges: [{
        chargeId: {
            type: String,
            required: true
        }
    }],
    balance: {
        type: Number,
        required: true
    },
    transactions: [{
        userId: {
            type: String,
            required: true
        },
        transactionId: {
            type: String,
            required: true
        },
        customerId: { type: String }
    }],
    disputes: [{
        disputeId: {
            type: String,
            required: true
        },
        chargeId: {
            type: String,
            required: true
        }
    }],
    refunds: [{
        refundId: {
            type: String,
            required: true
        },
        chargeId: {
            type: String,
            required: true
        }
    }],
    coupons: [{
        couponId: {
            type: String,
            required: true
        }
    }],
    dateCreated: {
        type: Date,
        default: Date.now,
        required: true
    },
    dateUpdated: {
        type: Date,
        default: Date.now,
        required: true
    }
})


// Exports the payments model schema
module.exports = mongoose.model('Transaction', TransactionSchema)