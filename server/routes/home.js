const Router = require('koa-router')
const { version } = require('../../config')

/** @class {Object} HomeRouter
 * @param {Object} options
 * handles GET HomeRoutes -> ['/home/', '/version/home/', '/', 'version/']
 */
class HomeRouter extends Router {
    constructor(options = {}) {
        super()
        this.options = options
        this.middleware = new options.Middleware()
    }
    // Initializes the application route
    init(app) {
        this.get('/home', ctx => this.home(ctx))
        this.get(`${version}/home`, ctx => this.home(ctx))

        app.use(async (ctx, next) => await this.middleware.handleAuth(ctx, next))
        app.use(this.routes())
    }

    /**
     * @param {ctx} ctx route context object
     * home/index route of the application
     */
    async home(ctx) {
        ctx.body = {
            message: 'welcome to pocketloan-transactions',
            status: 200
        }
    }
}

// exports the Homerouter class
module.exports = HomeRouter