const Router = require('koa-router')
const { isEmpty, merge } = require('lodash')
const { version } = require('../../config')
const { SERVER_ERROR, BAD_REQUEST, UNKNOWN_ERROR } = require('../errorMessages')

/**
 * Transaction Router Class
 * Routes Api Requests to its appropriate service method
 * @constructor {Object} options
 * @param {Object} constructor - [transactionService, NotificationService, TransactionDao]
 */
class TransactionRouter extends Router {
    constructor({ TransactionService, NotificationService, TransactionDao }) {
        super()
        this.transactionDao = new TransactionDao()
        this.transactionService = new TransactionService()
        this.notificationService = new NotificationService()
    }

    // Initializes the application route
    // initializes the router
    init(app) {
        // POST REQUESTS
        this.prefix(`${version}`)
        this.post('/', ctx => this.createTransaction(ctx))
        this.post('/charges', ctx => this.createCharge(ctx))
        this.post('/customers', ctx => this.createCustomer(ctx))
        this.post('/payouts', ctx => this.createPayout(ctx))
        this.post('/payouts/cancel', ctx => this.cancelPayout(ctx))
        this.post('/accounts', ctx => this.createAccount(ctx))
        this.post('/tokens', ctx => this.createToken(ctx))
        this.post('/transfers', ctx => this.createTransfer(ctx))
        this.post('/externalAccounts', ctx => this.createExternalAccount(ctx))

        // GET REQUESTS
        this.get('/', ctx => this.retrieveTransactions(ctx))
        this.get('/charges', ctx => this.retrieveCharges(ctx))
        this.get('/customers', ctx => this.retrieveCustomers(ctx))
        this.get('/balances', ctx => this.retrieveBalances(ctx))
        this.get('/balance', ctx => this.retrieveBalance(ctx))
        this.get('/payouts', ctx => this.retrievePayouts(ctx))
        this.get('/transfers', ctx => this.retrieveTransfers(ctx))
        this.get('/accounts', ctx => this.retrieveAccounts(ctx))

        this.get('/:id', ctx => this.retrieveTransactionById(ctx))
        this.get('/users/:id', ctx => this.retrieveUserById(ctx))
        this.get('/tokens/:id', ctx => this.retrieveTokenById(ctx))
        this.get('/charges/:id', ctx => this.retrieveChargeById(ctx))
        this.get('/customers/:id', ctx => this.retrieveCustomerById(ctx))
        this.get('/payouts/:id', ctx => this.retrievePayoutById(ctx))
        this.get('/accounts/:id', ctx => this.retrieveAccountById(ctx))
        this.get('/balances/:id', ctx => this.retrieveBalanceByAccountId(ctx))
        this.get('/transfers/:id', ctx => this.retrieveTransferById(ctx))

        // PATCH / UPDATE REQUESTS
        this.patch('/:id', ctx => this.updateTransaction(ctx))
        this.patch('/charges/:id', ctx => this.updateCharge(ctx))
        this.patch('/customers/:id', ctx => this.updateCustomer(ctx))
        this.patch('/payouts/:id', ctx => this.updatePayout(ctx))
        this.patch('/accounts/:id', ctx => this.updateAccount(ctx))
        this.patch('/transfers/:id', ctx => this.updateTransfer(ctx))

        // DELETE REQUESTS
        this.delete('/:id', ctx => this.deleteTransaction(ctx))
        this.delete('/customers/:id', ctx => this.deleteCustomer(ctx))
        this.delete('/accounts/:id', ctx => this.deleteAccount(ctx))

        // initializes routes
        app.use(this.routes())
    }

    /**
     * Creates a new transaction linked to a user Id
     * @param {String} ctx[request][body][userId]- Users Id
     * @param {String} ctx[request][body][balance] - Sets users balance
     * @returns {Object} Koa context body
     */
    async createTransaction(ctx) {
        try {
            const { userId, balance, country, email, type } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const { _id } = await this.transactionDao.createTransaction({ userId, balance })
            const account = await this.transactionService.createAccount({ country, email, type })
            const customer = await this.transactionService.createCustomer({ account_balance: balance, email })
            await this.transactionDao.updateTransaction(userId, ['customerId', customer.id])
            await this.transactionDao.updateTransaction(userId, { accounts: { accountId: account.id } })
            const transaction = await this.transactionDao.retrieveTransactionById(_id)
            ctx.body = {
                status: 200,
                message: 'successfully created a new transaction',
                transaction
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Creates a charge deducted from the users bankAccount or card
     * @param {String} ctx[reques][body][userId]- Users Id
     * @param {String} ctx[request][body][currency] - Currency of the transaction i.e 'cad' 'usd'
     * @param {Number} ctx[reques][body][ammount]- Amount to be deducted from users bank account or card
     * @param {Object} ctx[request][body][transaction] - The transaction object
     * @returns {Object} Koa context body
     */
    async createCharge(ctx) {
        try {
            const { userId, amount, currency, transaction } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const charge = await this.transactionService.createCharge(transaction, { amount, currency })
            await this.transactionDao.updateTransaction(userId, { charges: { chargeId: charge.id } })
            ctx.body = {
                message: `user ${userId} was successfully charged ${amount} ${currency}`,
                status: 200,
                charge
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Creates a new customer for easier recurring charges
     * @param {String} ctx[request][body][userId]- Users Id
     * @param {Object} ctx[request][body][transaction] - The transaction object
     * @returns {Object} Koa context body
     */
    async createCustomer(ctx) {
        try {
            const { userId, transaction } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const customer = await this.transactionService.createCustomer(transaction.id)
            await this.transactionDao.updateTransaction(userId, ['customerId', customer.id])
            ctx.body = {
                message: `successfully created new customer`,
                status: 200,
                customer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Creates a payout i.e sends money to an external bank
     * @param {String} ctx[request][body][userId]- Users Id
     * @param {String} ctx[request][body][currency] - Currency of the transaction i.e 'cad' 'usd'
     * @param {Number} ctx[request][body][ammount]- Amount to be sent to a particular user
     * @param {String} ctx[request][body][destination]- Bank account or card id where money should be sent to
     * @returns {Object} Koa context body
     */
    async createPayout(ctx) {
        try {
            const { userId, amount, currency, destination, sourceType, stripeAccountId } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const payout = await this.transactionService.createPayout({
                amount,
                currency,
                destination,
                source_type: sourceType,
                stripeAccount: {
                    stripe_account: stripeAccountId
                }
            })
            await this.transactionDao.updateTransaction(userId, { payouts: { payoutId: payout.id } })
            ctx.body = {
                message: `successfully created payout`,
                status: 200,
                payout
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Cancels payout sent to an external bank account
     * @param {String} ctx[request][body][payoutId]- Payout Id
     * @returns {Object} Koa context body
     */
    async cancelPayout(ctx) {
        try {
            const { userId, payoutId } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const payout = await this.transactionService.cancelPayout(payoutId)
            ctx.body = {
                message: `successfully created new customer`,
                status: 200,
                payout
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    async createAccount(ctx) {
        try {
            const { userId, country, type, email } = ctx.request.body
            if (isEmpty(userId)) {
                throw new Error(`expected userId but got ${typeof userId}`)
            }
            const account = await this.transactionService.createAccount({ type, country, email })
            await this.transactionDao.updateTransaction(userId, { accounts: { accountId: account.id } })
            ctx.body = {
                message: `successfully created new customer`,
                status: 200,
                account
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Retrieves all transaction history from our database
     * @param {String} ctx - Koa context
     * @returns {Object} Koa context body
     */
    async retrieveTransactions(ctx) {
        try {
            const transactions = await this.transactionDao.retrieveTransactions()
            ctx.body = {
                status: 200,
                message: 'successfully retrieved transactions',
                transactions
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = SERVER_ERROR.status
            ctx.body = SERVER_ERROR
            throw err
        }
    }

    /**
     * Creates a bank or card token
     * @param {String} ctx - Koa context
     * @returns {Object} Koa context body
     */
    async createToken(ctx) {
        try {
            const token = await this.transactionService.createToken(ctx.request.body)
            ctx.body = {
                status: 200,
                message: `successfully created a new ${ctx.request.body.tokenType} token`,
                token
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Creates a stripe tranfer between 2 stripe connected accounts
     * @param {Number} ctx[request][body][amount] - amount to be transfered
     * @param {String} ctx[request][body][currency] - currency of the funds beign moved
     * @param {String} ctx[request][body][destination] - destination (connected stripe account) of the funds beign moved
     * @returns {Object} Koa context body
     */
    async createTransfer(ctx) {
        try {
            const transfer = await this.transactionService.createTransfer(ctx.request.body)
            ctx.body = {
                status: 200,
                message: `successfully created a new stripe transfer`,
                transfer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Creates an external accounts to recieve payouts
     * @param {String} ctx[request][body][bankAccountToken]- Users bank account token
     * @param {String} ctx[request][body][accountId] - Users stripe account account ID
     * @returns {Object} Koa context body
     */
    async createExternalAccount(ctx) {
        try {
            const { accountId, bankAccountToken } = ctx.request.body
            const externalAccount = await this.transactionService.createExternalAccount(accountId, bankAccountToken)
            ctx.body = {
                status: 200,
                message: `successfully created a new external account for ${accountId}`,
                externalAccount
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves all previous charges restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of charges history
     * @returns {Object} Koa context body
     */
    async retrieveCharges(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrieveCharges(limit)
            ctx.body = {
                message: 'successfully retrieved all previous charges',
                status: 200,
                charges: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err

        }
    }

    /**
     * Retrieves all customers restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of customers
     * @returns {Object} Koa context body
     */
    async retrieveCustomers(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrieveCustomers(limit)
            ctx.body = {
                message: 'successfully retrieved all customers',
                status: 200,
                customers: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves all previous balances restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of balance history
     * @returns {Object} Koa context body
     */
    async retrieveBalances(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrieveBalanceTransactions(limit)
            ctx.body = {
                message: 'successfully retrieved all balance transactions',
                status: 200,
                balances: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves our stripe current balance 
     * @param {String} ctx[request] - Koa Request Object
     * @returns {Object} Koa context body
     */
    async retrieveBalance(ctx) {
        try {
            const balance = await this.transactionService.retrieveBalance()
            ctx.body = {
                message: 'successfully retrieved all balance transactions',
                status: 200,
                balance
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves all previous payouts restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of payout history
     * @returns {Object} Koa context body
     */
    async retrievePayouts(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrievePayouts(limit)
            ctx.body = {
                message: 'successfully retrieved all previous payouts',
                status: 200,
                payouts: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves all previous transfers restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of transfer history
     * @returns {Object} Koa context body
     */
    async retrieveTransfers(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrieveTransfers(limit)
            ctx.body = {
                message: 'successfully retrieved all previous transfers',
                status: 200,
                transfers: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves all accounts restricted by limit (default is 10)
     * @param {String} ctx[request][body][limit] - limit of accounts
     * @returns {Object} Koa context body
     */
    async retrieveAccounts(ctx) {
        try {
            const { limit } = ctx.query
            const { data } = await this.transactionService.retrieveAccounts(limit)
            ctx.body = {
                message: 'successfully retrieved all accounts',
                status: 200,
                accounts: data
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular transaction by id
     * @param {String} ctx[request][body][id] - Transaction Id
     * @returns {Object} Koa context body (Single Transaction)
     */
    async retrieveTransactionById(ctx) {
        try {
            const { id } = ctx.params
            const transaction = await this.transactionDao.retrieveTransactionById(id)
            ctx.body = {
                status: 200,
                message: 'successfully retrieved transaction by id',
                transaction
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular user by id
     * @param {String} ctx[request][body][id] - User Id
     * @returns {Object} Koa context body (Single User)
     */
    async retrieveUserById(ctx) {
        try {
            const { id } = ctx.params
            if (isEmpty(id)) {
                throw new Error(`Expected user id to be a valid string but got ${typeof id}`)
            }
            const user = await this.transactionDao.retrieveUserById(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved user by id`,
                user
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular stripe user card token by id
     * @param {String} ctx[params] - Token Id
     * @returns {Object} Koa context body
     */
    async retrieveTokenById(ctx) {
        try {
            const { id } = ctx.params
            if (isEmpty(id)) {
                throw new Error(`Expected token id to be a valid string but got ${typeof id}`)
            }
            const token = await this.transactionService.retrieveToken(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved token by id`,
                token
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular charge by id
     * @param {String} ctx[request][body][id] - User Id
     * @returns {Object} Koa context body (Charge Object)
     */
    async retrieveChargeById(ctx) {
        try {
            const { id } = ctx.params
            if (isEmpty(id)) {
                throw new Error(`Expected charge id to be a valid string but got ${typeof id}`)
            }
            const charge = await this.transactionService.retrieveCharge(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved charge by id`,
                charge
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular customer by id
     * @param {String} ctx[request][body][id] - User Id
     * @returns {Object} Koa context body (Customer Object)
     */
    async retrieveCustomerById(ctx) {
        try {
            const { id } = ctx.params
            if (isEmpty(id)) {
                throw new Error(`Expected customer id to be a valid string but got ${typeof id}`)
            }
            const customer = await this.transactionService.retrieveCustomer(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved customer by id`,
                customer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular payout by id
     * @param {String} ctx[request][body][id] - User Id
     * @returns {Object} Koa context body (Payout Object)
     */
    async retrievePayoutById(ctx) {
        try {
            const { id } = ctx.params
            if (isEmpty(id)) {
                throw new Error(`Expected payout id to be a valid string but got ${typeof id}`)
            }
            const payout = await this.transactionService.retrievePayout(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved payout by id`,
                payout
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular account by id
     * @param {String} ctx[request][body][id] - User Id
     * @returns {Object} Koa context body (Account Object)
     */
    async retrieveAccountById(ctx) {
        try {
            const { id } = ctx.params
            const account = await this.transactionService.retrieveAccount(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved account by id`,
                account
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a users account balance by account id
     * @param {String} ctx[params][id] - users account id
     * @returns {Object} Koa context body (Balance Object)
     */
    async retrieveBalanceByAccountId(ctx) {
        try {
            const { id } = ctx.params
            const balance = await this.transactionService.retrieveBalance(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved balance by account id`,
                balance
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular transfer by id
     * @param {String} ctx[params][id] - transfer id
     * @returns {Object} Koa context body (Transfer Object)
     */
    async retrieveTransferById(ctx) {
        try {
            const { id } = ctx.params
            const transfer = await this.transactionService.retrieveTransfer(id)
            ctx.body = {
                status: 200,
                message: `successfully retrieved transfer by id`,
                transfer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Retrieves a particular user by id
     * @param {String} ctx[request][body] - Field specified
     * @returns {Object} Koa context body (Transaction(s))
     */
    async retrieveTransaction(ctx) {
        try {
            const req = ctx.request.body
            const transactions = await this.transactionDao.retrieveTransaction(req)
            ctx.body = {
                status: 200,
                message: `successfully retrieved transaction by ${req}`,
                transactions
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     *  a particular transaction by id
     * @param {String} ctx[params][id] - Transaction Id
     * @param {String} ctx[request][body] - Fields to be updated
     * @returns {Object} Koa context body
     */
    async updateTransaction(ctx) {
        try {
            const { id } = ctx.params
            await this._updateAccount(merge({ id, user_agent: ctx.request['header']['user-agent'] }, ctx.request.body))
            const transaction = await this.transactionDao.updateTransaction(id, ctx.request.body)
            ctx.body = {
                status: 200,
                message: `successfully updated transaction`,
                transaction
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    // async updateCharge(ctx) { }
    // async updateCustomer(ctx) { }
    // async updatePayout(ctx) { }

    /**
     *  a particular transaction by id
     * @param {String} ctx[params][id] - Transaction Account Id
     * @param {String} ctx[request][body] - Fields to be updated
     * @returns {Object} Koa context body
     */
    async updateAccount(ctx) {
        const { id } = ctx.params
        try {
            const account = await this.transactionService.updateAccount(id, ctx.request.body)
            ctx.body = {
                status: 200,
                message: `successfully updated user transaction account`,
                account
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     *  Updates a particular transfer by id
     * @param {String} ctx[params][id] - Transfer Id
     * @param {String} ctx[request][body] - Fields to be updated
     * @returns {Object} Koa context body
     */
    async updateTransfer(ctx) {
        const { id } = ctx.params
        try {
            const transfer = await this.transactionService.updateTransfer(id, ctx.request.body)
            ctx.body = {
                status: 200,
                message: `successfully updated transfer by id`,
                transfer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = BAD_REQUEST.status
            ctx.body = BAD_REQUEST
            throw err
        }
    }

    /**
     * Deletes a particular transaction by id
     * @param {String} ctx[request][body][id] - Transaction Id
     * @returns {Object} Koa context body
     */
    async deleteTransaction(ctx) {
        try {
            const { id } = ctx.params
            const deleted = await this.transactionDao.deleteTransaction(id)
            ctx.body = {
                status: 200,
                message: 'successfully deleted transaction by id',
                deleted
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = UNKNOWN_ERROR.status
            ctx.body = UNKNOWN_ERROR
            throw err
        }
    }

    /**
     * Deletes a particular customer by id
     * @param {String} ctx[request][body][id] - Customer Id
     * @returns {Object} Koa context body
     */
    async deleteCustomer(ctx) {
        try {
            const { id } = ctx.params
            const customer = await this.transactionService.deleteCustomer(id)
            ctx.body = {
                status: 200,
                message: 'successfully deleted customer by id',
                customer
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = UNKNOWN_ERROR.status
            ctx.body = UNKNOWN_ERROR
            throw err
        }
    }

    /**
     * Deletes a particular account by id
     * @param {String} ctx[request][body][id] - account Id
     * @returns {Object} Koa context body
     */
    async deleteAccount(ctx) {
        try {
            const { id } = ctx.params
            const account = await this.transactionService.deleteAccount(id)
            ctx.body = {
                status: 200,
                message: 'successfully deleted customer by id',
                account
            }
        } catch (err) {
            await this.notificationService.slackBot(`Oops! an error just occured in pocketloan-transactions err: ${err.stack.toString()}`)
            ctx.status = UNKNOWN_ERROR.status
            ctx.body = UNKNOWN_ERROR
            throw err
        }
    }

    async _updateAccount(request) {
        if (!isEmpty(request.cardTokens) || !isEmpty(request.bankAccounts)) {
            const { id } = request
            const response = await this.transactionDao.retrieveUserById(id)
            let transaction, accountId, updated
            if (!isEmpty(request.cardTokens)) {
                const { _id, transactionId, firstName, lastName, email, customerId } = response.user
                if (isEmpty(customerId)) {
                    const customer = await this.transactionService.createCustomer({ account_balance: 0, email })
                    await this.transactionDao.updateTransaction(_id, ['customerId', customer.id])
                    await this.transactionService.updateCustomer(customer.id, { source: request.cardTokens.cardTokenId })
                } else {
                    await this.transactionService.updateCustomer(customerId, { source: request.cardTokens.cardTokenId })
                }
                transaction = await this.transactionDao.retrieveTransactionById(transactionId)
                accountId = transaction.accounts[0].accountId
                const { client_ip, created, user_agent } = request
                updated = await this.transactionService.updateAccount(accountId, {
                    tos_acceptance: {
                        date: created,
                        ip: client_ip,
                        user_agent
                    },
                    legal_entity: {
                        first_name: firstName,
                        last_name: lastName,
                        type: 'individual'
                    },
                    payout_schedule: {
                        interval: 'manual'
                    }
                })
            }
            if (!isEmpty(request.bankAccounts)) {
                const { transactionId, firstName, lastName } = response.user
                transaction = await this.transactionDao.retrieveTransactionById(transactionId)
                accountId = transaction.accounts[0].accountId
                await this.transactionService.createExternalAccount(accountId, request.bankAccounts.bankAccountTokenId)
                const { client_ip, created, user_agent } = request
                updated = await this.transactionService.updateAccount(accountId, {
                    tos_acceptance: {
                        date: created,
                        ip: client_ip,
                        user_agent
                    },
                    legal_entity: {
                        first_name: firstName,
                        last_name: lastName,
                        type: 'individual'
                    },
                    payout_schedule: {
                        interval: 'manual'
                    }
                })
            }
        }
    }
}

// exports the TransactionRouter
module.exports = TransactionRouter