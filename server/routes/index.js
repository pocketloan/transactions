const HealthRouter = require('./health')
const HomeRouter = require('./home')
const OpenApiRouter = require('./openapi')
const TransactionRouter = require('./transaction')
const TransactionService = require('../services/transaction')
const TransactionDao = require('../dao/transactions')
const NotificationService = require('../services/notification')
const Middleware = require('../services/middleware')

// Exports and Initializes the application routes
module.exports = (app) => {
    const handlers = [
        new HealthRouter(),
        new HomeRouter({ Middleware }),
        new OpenApiRouter(),
        new TransactionRouter({
            TransactionService,
            NotificationService,
            TransactionDao
        })
    ]

    handlers.forEach(handler => {
        handler.init(app)
    })
}