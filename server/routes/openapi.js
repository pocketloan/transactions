const Router = require('koa-router')
const yaml = require('yamljs')
const { version } = require('../../config')

/**
 * @class {Object} OpenApiRouter
 * @constructor {Object} options
 * @desc  Displays api documentation of the application
 */
class OpenApiRouter extends Router {
    constructor(options = {}) {
        super()
        this.options = options
    }
    // Initializes the route
    init(app) {
        this.get('/openapi', ctx => this.api(ctx))
        this.get(`${version}/openapi`, ctx => this.api(ctx))

        app.use(this.routes())
    }
    /**
     * @param {ctx} ctx route context object
     * @desc  route to display the api documentation of he application
     */
    async api(ctx) {
        ctx.body = yaml.load('./api-docs.yml')
        ctx.status = 200
    }

}

// exports the OpenApiRouter
module.exports = OpenApiRouter