// Require dependencies
const mongoose = require('mongoose')
const { uri } = require('../../config')

/**
 * @class Mongo
 * @param {Object} Options
 * establishes database connection and checks app health
 */
class Mongo {
    constructor(options = {}) {
        this.options = options
    }

    /**
     * @param  {Null}
     * @return {Bool} boolean
     * method to connect to the database, returns a boolean
     * if connection is established
     */
    async connect() {
        return mongoose.connect(uri, { useNewUrlParser: true })
    }
    /**
     * @param  {Null}
     * @return {Object} options
     * return database connection options
     */
    async databaseOptions() {
        return this.options
    }
}

// Exports the Mongo class
module.exports = Mongo
