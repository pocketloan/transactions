// Require dependencies
require('../models/transaction')
const _ = require('lodash')
const axios = require('../utils/axios')
const Mongoose = require('mongoose')
const Mongo = require('./mongo')
const { pocketloanApi, version, hasAccess } = require('../../config')


/** 
 * Connects and fetches data from the database
 * All database requests should be gotten with the TransactionDao
 * @class {Object} Transactions Data Access Object
 * @param {Object} options
 */
class TransactionDoa {
    constructor(options = {}) {
        this.mongo = new Mongo()
        this.mongoose = Mongoose
        this.transaction = this.mongoose.model('Transaction')
        this.connectToDatabase()
    }

    /**
     * Retrieves a transaction by Id
     * @param {String} userId - Users unique id
     * @returns {Object} Transaction Object
     */
    async retrieveUserById(userId) {
        const { sub, subType, iss } = hasAccess[1]
        return axios.get(`${pocketloanApi}${version}/users/${userId}?sub=${sub}&subType=${subType}&iss=${iss}`)
            .then(res => res.data)
            .catch(err => { throw err })
    }

    /**
     * Retrieves all transactions
     * @returns {[Object]} List of transaction objects
     */
    async retrieveTransactions() {
        return this.transaction.find({}, (err, users) => {
            this._handleError(err)
            return users
        })
    }

    /**
     * Retrieves all transactions
     * @param {String} userId - User unique id
     * @param {Number} balance - Users balance. important!! -> expects the balance to not be passed in unless on special cases
     * @returns {Object} A new transaction object
     */
    async createTransaction({ userId, balance = 0 }) {
        const newTransaction = new this.transaction({ userId, balance })
        newTransaction.dateCreated = new Date().toISOString()
        newTransaction.dateUpdated = newTransaction.dateCreated
        return newTransaction.save().then(transaction => transaction).catch(err => { throw err })
    }

    /**
    * Retrieves all transactions
    * @param {String} id - Transaction id
    * @param {Number} balance - Users balance. important!! -> expects the balance to not be passed in unless on special cases
    * @returns {Object} A new transaction object
    */
    async retrieveTransactionById(id) {
        return this.transaction.findById(id, (err, user) => {
            this._handleError(err)
            return user
        })
    }

    /**
     * Retrieves a transaction by any field(s)
     * @param {Object} query - fileds queried to retrieve transaction object
     * @returns {Object} Transaction Object
     */
    async retrieveTransaction(query) {
        return this.transaction.findOne(query, (err, user) => {
            this._handleError(err)
            return user
        })
    }

    /**
     * Deletes a transaction. Important!! you dont want to do this unless user deletes his/her pocketloan account
     * @param {Object} _id - Transaction id
     */
    async deleteTransaction(_id) {
        return this.transaction.deleteOne({ _id }, err => this._handleError(err))
    }

    /**
     * Updates a transaction by any field queried
     * @param {String} id - Transaction id
     * @returns {Object} Updated transaction Object
     */
    async updateTransaction(id, fields) {
        if (_.isArray(fields)) {
            return this.transaction.findOne({ userId: id }, (err, transaction) => {
                this._handleError(err)
                transaction['dateUpdated'] = new Date().toISOString()
                transaction[fields[0]] = fields[1]
                transaction.save((err, updated) => {
                    this._handleError(err)
                    return updated
                })
            })
        }
        return this.transaction.update({ userId: id }, { $addToSet: fields }, err => this._handleError(err))
    }

    /**
     * @return {Object} MongooseConnection
     * method to connect users to the database
     */
    async connectToDatabase() {
        return await this.mongo.connect()
    }

    /**
     * Handles all errors in the Transaction
     * @param {Error} err - error object instance
     * @throws {Error}
     */
    _handleError(err) {
        if (err) {
            throw err
        }
    }
}

// exports the TransactionDao
module.exports = TransactionDoa