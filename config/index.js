const hasAccess = require('../server/utils/allowedServices')
const PORT = process.env.PORT || 3001

module.exports = {
    PORT,
    uri: process.env.MONGO_URI,
    version: process.env.VERSION,
    jwtSecret: process.env.JWT_SECRET,
    pocketloanNotificationService: process.env.POCKETLOAN_NOTIFY,
    pocketloanApi: process.env.POCKETLOAN_API,
    pocketloanActivity: process.env.POCKETLOAN_ACTIVITY,
    env: (req) => (req.header.host.includes(`localhost:${PORT}`)) ? 'development' : 'production',
    hasAccess,
    stripeSecretKey: process.env.STRIPE_KEY
}